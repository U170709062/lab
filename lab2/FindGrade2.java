import java.util.Scanner;
public class FindGrade2 {
	public static void main(String[] args) {
		int not2 = Integer.parseInt(args[0]);
		if(not2 > 100 || not2 < 0) {
			System.out.println("Hata, l�tfen kontrol et.");
		}
		else {
			if(not2 >= 90) {
				System.out.println("Notunuz: A");
			}
			else if(not2 >= 80) {
				System.out.println("Notunuz: B");
			}
			else if(not2 >= 70) {
				System.out.println("Notunuz: C");
			}
			else if(not2 >= 60) {
				System.out.println("Notunuz: D");
			}
			else if(not2 >= 50) {
				System.out.println("Notunuz: F");
			}
			else {
				System.out.println("Notunuz: FF");
			}
		}
	}
}