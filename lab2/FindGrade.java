import java.util.Scanner;
public class FindGrade {
	public static void main(String[] args) {
		Scanner not = new Scanner(System.in);
		System.out.println("Sayı Giriniz:");
		int not2 = not.nextInt();
		if(not2 > 100 || not2 < 0) {
			System.out.println("Hata, lütfen kontrol et.");
		}
		else {
			if(not2 >= 90) {
				System.out.println("Notunuz: A");
			}
			else if(not2 >= 80) {
				System.out.println("Notunuz: B");
			}
			else if(not2 >= 70) {
				System.out.println("Notunuz: C");
			}
			else if(not2 >= 60) {
				System.out.println("Notunuz: D");
			}
			else if(not2 >= 50) {
				System.out.println("Notunuz: F");
			}
			else {
				System.out.println("Notunuz: FF");
			}
		}
	}
}