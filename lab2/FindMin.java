public class FindMin {
	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		int c = Integer.parseInt(args[2]);
		
		int result;
		
		boolean someCondition = a < b;
		result = someCondition ? a : b;
		result = result < c ? result : c;
		System.out.println(result);
	}
}