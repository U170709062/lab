
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		
		Random randnum = new Random();
		int rand1 = randnum.nextInt(100);
		Scanner user1 = new Scanner(System.in);
		System.out.println("Can you guess the number between 0 to 99 ? : ");
		int number1 = user1.nextInt();
		int say = 0;
		while(number1 != rand1 && number1 != -1) {
			say = say + 1;
			if (number1 > rand1) {
				System.out.println("Wrong number, please enter the new number less than your guess or for exit write -1: ");
				number1 = user1.nextInt();
			}
			else {
				System.out.println("Wrong number, please enter the new number more than your guess or for exit write -1: ");
				number1 = user1.nextInt();
			}
		}
		System.out.println("Game is done. The number was " + rand1);
		if (number1 == rand1) {
		System.out.println("Congratulations, you have known the number "+ say + ". steps.");
		}
		else {
			System.out.println("Unfortunatelly, good by.");
		}
		
		}
	}
