import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class FindPrimeNumber {
    public static void main(String[] args) throws IOException {
    	int prime = Integer.parseInt(args[0]);
    	for(int i=1; i<=prime; i++) {
    		int error = 0;
    		for (int a=2; a<i; a++) {
    			int sonuc = i%a;
    			if (sonuc == 0) {
    				error = error+1;
    			}
    		}
    		if (error == 0) {
    			System.out.print(i + " ");
    		}
    	}
    }
}